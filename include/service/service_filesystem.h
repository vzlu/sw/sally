#ifndef _SERVICE_FILESYSTEM_BACKEND_H
#define _SERVICE_FILESYSTEM_BACKEND_H

#include <service/conf_srv.h>

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <time.h>

#define SERVICE_FS_ATTR_RO              (1UL << 1)
#define SERVICE_FS_ATTR_HIDDEN          (1UL << 2)
#define SERVICE_FS_ATTR_SYSTEM          (1UL << 3)
#define SERVICE_FS_ATTR_DIR             (1UL << 4)
#define SERVICE_FS_ATTR_ARCH            (1UL << 5)

// some default FS modes (mainly to provide a way for compatibility with ANSI C string modes)
#define SERVICE_FS_MODE_RDONLY          (1UL << 0)
#define SERVICE_FS_MODE_WRONLY          (1UL << 1)
#define SERVICE_FS_MODE_RDWR            (1UL << 2)
#define SERVICE_FS_MODE_CREAT           (1UL << 3)
#define SERVICE_FS_MODE_EXCL            (1UL << 4)
#define SERVICE_FS_MODE_TRUNC           (1UL << 5)
#define SERVICE_FS_MODE_APPEND          (1UL << 6)

typedef void* service_fs_file_handle_t;
typedef void* service_fs_dir_handle_t;

typedef struct {
  uint32_t size;
  char name[SERVICE_FS_MAX_FILENAME_LEN];
  uint32_t attr;
  struct tm mtime;
} service_fs_file_stat_t;

typedef struct {
  // access mode specifiers
  struct {
    int RDONLY;
    int WRONLY;
    int RDWR;
    int CREAT;
    int EXCL;
    int TRUNC;
    int APPEND;
  } mode;

  // the total available space
  uint32_t total_space; 

  // intialization result
  int stat_init;

  // last status recorded by check()
  int stat_last;

  // last non-OK status recorded by check()
  int stat_err;

  // support methods
  service_fs_file_handle_t (*file_handle_malloc)(void);
  void (*file_handle_free)(service_fs_file_handle_t handle);
  service_fs_dir_handle_t (*dir_handle_malloc)(void);
  void (*dir_handle_free)(service_fs_dir_handle_t handle);
  bool (*check)(int res);
  bool (*is_file)(service_fs_file_stat_t* stat);
  size_t (*size)(service_fs_file_handle_t handle);
  bool (*path_validate)(char* path);
  int (*init)(void** args);
  int (*mount)(void);
  int (*unmount)(void);
  int (*format)(void);

  // memory size methods
  uint32_t (*get_free_space)(void);
  uint32_t (*get_bad_blocks)(void);

  // POSIX-like methods
  int (*open)(service_fs_file_handle_t handle, const char* path, int mode);
  int (*close)(service_fs_file_handle_t handle);
  int (*stat)(const char* path, service_fs_file_stat_t* stat);
  int (*unlink)(const char* path);
  int (*mkdir)(const char* path, int mode);
  int (*rmdir)(const char* path);
  int (*rename)(const char* path_old, const char* path_new);
  size_t (*write)(service_fs_file_handle_t handle, const void* buff, size_t count);
  size_t (*read)(service_fs_file_handle_t handle, void* buff, size_t count);
  size_t (*tell)(service_fs_file_handle_t handle);
  int (*lseek)(service_fs_file_handle_t handle, size_t offset);
  int (*opendir)(service_fs_dir_handle_t handle, const char* path);
  int (*readdir)(service_fs_dir_handle_t handle, service_fs_file_stat_t* stat);
  int (*closedir)(service_fs_dir_handle_t handle);
  int (*flush)(service_fs_file_handle_t handle);
  int (*eof)(service_fs_file_handle_t handle);
  int (*chmod)(const char* path, uint32_t mode);
  int (*gc)(void);
  int (*wipe)(void);
} service_fs_t;

#endif
