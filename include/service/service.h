#ifndef _SERVICE_H
#define _SERVICE_H

#include <service/conf_srv.h>
#include "service_filesystem.h"
#include "service_os.h"

#if defined(SERVICE_CSP)
#include <csp/csp_types.h>

// alias for services without CSP port
#define SERVICE_CSP_PORT_UNUSED                 (-1)
#endif

// the maximum allowed number of registered callbacks
#define SERVICE_REG_CB_MAX                      (4)

// alias for "no thread"
#define SERVICE_THREAD_NONE                     { .attr = NULL }

// the final item in a list of service descriptors
#if defined(SERVICE_CSP)
#define SERVICE_DESC_LIST_TERMINATOR            { .port = SERVICE_CSP_PORT_UNUSED, .th_top = NULL, .th_spawned = NULL, .cb_packet_read = NULL, .cb_init = NULL }
#else
#define SERVICE_DESC_LIST_TERMINATOR            { .th_top = NULL, .th_spawned = NULL, .cb_packet_read = NULL, .cb_init = NULL }
#endif

#define SERVICE_LOGGING_LEVEL_UNUSED            (-1)
#define SERVICE_LOGGING_UNUSED                  { SERVICE_LOGGING_LEVEL_UNUSED, SERVICE_LOGGING_LEVEL_UNUSED }
#define SERVICE_LOGGING_LEVELS_MAX              (2)

typedef int (*cb_execution_func_t)(void** args);
typedef void (*cb_registration_func_t)(cb_execution_func_t cb, int index);
typedef void (*cb_fs_reg_func_t)(service_fs_t* fs);
#if defined(SERVICE_CSP)
typedef void (*on_csp_packet_read_cb_t)(csp_packet_t* packet, csp_conn_t* conn);
typedef void (*on_csp_conn_accepted_cb_t)(csp_conn_t* conn);
#endif
typedef void (*cb_log_setup_func_t)(uint32_t enable);

/*!
  \brief Container for threads
*/
typedef struct {
  /*!
    \brief Thread attributes.
      CMSIS:     osThreadAttr_t
      pthreads:  pthread_attr_t*
      FreeRTOS:  none - must be defined by user
  */
  service_os_thread_attr_t attr;

  /*!
    \brief Thread arguments to be passed to \ref func.
  */
  void* arg;
  
  /*!
    \brief Thread function.
      CMSIS:     osThreadFunc_t
      pthreads:  void*
      FreeRTOS:  void*
  */
  service_os_thread_func_t func;

  /*!
    \brief Thread handle or ID.
      CMSIS:     osThreadId_t
      pthreads:  pthread_t*
      FreeRTOS:  TaskHandle_t*
  */
  service_os_thread_handle_t handle;
} service_thread_t;

/*!
  \brief Service decription structure.
*/
typedef struct {
  #if defined(SERVICE_CSP)
  /*! 
    \brief CSP port number (-1 for unused)
  */
  const int port;
  #endif

  /*!
    \brief Array of logging level flags.
    Each entry is a different logging level, enabled by a different flag (e.g., from persistent storage).
  */
  int log_flag_index[SERVICE_LOGGING_LEVELS_MAX];

  /*! 
    \brief Top-level thread.
  */
  service_thread_t th_top;

  /*! 
    \brief Template to use for all spawned threads.
  */
  service_thread_t th_spawned;

  /*! 
    \brief Array of callbacks for logging setup.
    Position in array corresponds to the logging level in \ref log_flag_index.
  */
  cb_log_setup_func_t cb_log_setup[SERVICE_LOGGING_LEVELS_MAX];
  #if defined(SERVICE_CSP)
  /*! 
    \brief Function to be called when CSP packet is read from a connection.
  */
  on_csp_packet_read_cb_t cb_packet_read;

  /*! 
    \brief Function to be called when CSP connection is accepted.
  */
  on_csp_conn_accepted_cb_t cb_conn_accepted;
  #endif

  /*! 
    \brief Function to be called when creating the service via \ref service_spawn.
  */
  cb_execution_func_t cb_init;

  /*! 
    \brief Arguments to pass to the init function callback.
  */
  void** cb_init_args;

  /*!
    \brief A callback to function implemented by service, which takes array of callbacks as an argument.
    This allows services to call functions implemeted within the application.
  */
  cb_registration_func_t cb_reg;

  /*!
    \brief An array of callbacks to functions implemented by application, which do the actual work.
    This allows services to call functions implemeted within the application.
  */
  cb_execution_func_t cbs[SERVICE_REG_CB_MAX];

  /*!
    \brief Filesystem backend to be used by the service.
  */
  service_fs_t* fs_backend;

  /*!
    \brief A callback to function implemented by service, which registers \ref fs_backend.
  */
  cb_fs_reg_func_t fs_register;
} service_desc_t;

/*!
  \brief Check whether the provided service is a terminator.
  \param service Pointer to the service description to check.
  \returns True if the service is a terminator, false otherwise.
*/
bool service_is_terminator(service_desc_t* service);

/*!
  \brief Calculate service list length.
  \param service_list Pointer to the service list (array of services).
  \returns Length of the service list.
*/
int service_list_length(service_desc_t** service_list);

#if defined(SERVICE_CSP)
/*!
  \brief Register CSP callback functions - called during service initialization from app.
  This is separate from "service_spawn" because it operates over the entire service list.
  \param service_list Pointer to the service list (array of services).
*/
void service_register_csp_callbacks(service_desc_t** service_list);
#endif

/*!
  \brief Setup logging for a given service based on provided flags.
  \param service Pointer to the service.
  \param flags Flags to decide whether the service logging should be enabled or not.
  Decision is done based on the values on service_desc_t.log_flag_index.
*/
void service_setup_logging(service_desc_t* service, uint32_t flags);

#if defined(SERVICE_CSP)
/*!
  \brief Execute the CSP packet read callback. Called by CSP server from application
  each time a packet to the specified port is received.
  \param port CSP port to bind the callback to.
  \param packet Pointer to the packet which will be passed to the callback.
  \param conn Pointer to the connection which will be passed to the callback.
  \returns True if the callback was executed (regardless of outcome!), false otherwise.
*/
bool service_exec_read_callback(int port, csp_packet_t* packet, csp_conn_t* conn);

/*!
  \brief Execute the CSP packet connection accepted. Called by CSP server from application
  each time a connection to the specified port is accepted.
  \param port CSP port to bind the callback to.
  \param conn Pointer to the connection which will be passed to the callback.
  \returns True if the callback was executed (regardless of outcome!), false otherwise.
*/
bool service_exec_accept_callback(csp_conn_t* conn);
#endif

/*!
  \brief Spawn the service, based on provided description.
  \param service Pointer to the service description to spawn.
  \returns \ref err_codes
*/
int service_spawn(service_desc_t* service);

/*!
  \brief Function to execute a registered callback without arguments.
  Usually called from service implementation
  \param cbs Pointer to the array of callbacks registered within the service.
  \param index Index of the callback to execute.
  \returns \ref err_codes
*/
int service_exec_cb(cb_execution_func_t* cbs, int index);

/*!
  \brief Function to execute a registered callback with arguments.
  Usually called from service implementation
  \param cbs Pointer to the array of callbacks registered within the service.
  \param index Index of the callback to execute.
  \param args Array of arguments.
  \returns \ref err_codes
*/
int service_exec_cb_args(cb_execution_func_t* cbs, int index, void** args);

#endif