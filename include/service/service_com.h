#ifndef _SERVICE_COM_BACKEND_H
#define _SERVICE_COM_BACKEND_H

#include <stddef.h>
#include <stdint.h>

typedef void* service_com_handle_t;
typedef void* service_com_device_t;

typedef struct {
  uint32_t speed;
  uint32_t timeout_ms;
} service_com_conf_t;

typedef struct {
  int last_err;
  int (*open)(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf);
  int (*close)(service_com_handle_t handle);
  size_t (*write)(service_com_handle_t handle, const void* buff, size_t count);
  size_t (*read)(service_com_handle_t handle, void* buff, size_t count);
  size_t (*available)(service_com_handle_t handle);
  int (*flush)(service_com_handle_t handle);
  service_com_handle_t (*com_handle_malloc)(void);
  void (*com_handle_free)(service_com_handle_t handle);
} service_com_t;

#endif
