#ifndef _SERVICE_OS_BACKEND_H
#define _SERVICE_OS_BACKEND_H

#include <stddef.h>
#include <limits.h>

#define SERVICE_OS_PASS         (0)
#define SERVICE_OS_FAIL         (-1)
#define SERVICE_OS_MAX_DELAY    (ULONG_MAX)

// wait it's all just void pointers?
// always has been
typedef void* service_os_thread_handle_t;
typedef void* service_os_thread_attr_t;
typedef void* service_os_thread_func_t;
typedef void* service_os_thread_arg_t;
typedef void* service_os_mutex_handle_t;
typedef void* service_os_queue_handle_t;

// result type
typedef int service_os_res_t;

typedef struct {
  // threads
  service_os_res_t (*thread_create)(service_os_thread_handle_t* handle, service_os_thread_attr_t attr, service_os_thread_func_t func, service_os_thread_arg_t arg);
  void (*thread_delay)(unsigned long delay_ms);
  void (*thread_delay_until)(unsigned long* prev_ms, unsigned long delay_ms);
  unsigned long (*thread_get_time_count)(void);
  service_os_res_t (*thread_suspend)(service_os_thread_handle_t handle);
  service_os_res_t (*thread_resume)(service_os_thread_handle_t handle);
  service_os_res_t (*thread_delete)(service_os_thread_handle_t handle);
  service_os_res_t (*thread_wait_for)(service_os_thread_handle_t handle);
  service_os_thread_handle_t (*thread_find)(char* name);
  service_os_res_t (*is_inside_interrupt)(void);

  // memory
  void* (*malloc)(size_t size);
  void (*free)(void* buff);

  // mutex
  service_os_mutex_handle_t (*mutex_create)(void);
  service_os_res_t (*mutex_lock)(service_os_mutex_handle_t handle, unsigned long timeout_ms);
  service_os_res_t (*mutex_unlock)(service_os_mutex_handle_t handle);

  // queue
  service_os_queue_handle_t (*queue_create)(size_t len, size_t size);
  service_os_res_t (*queue_send)(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms);
  service_os_res_t (*queue_receive)(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms);
  void (*queue_delete)(service_os_queue_handle_t handle);
  
} service_os_t;

void service_set_os(service_os_t* os);
service_os_res_t service_os_thread_create(service_os_thread_handle_t* handle, service_os_thread_attr_t attr, service_os_thread_func_t func, service_os_thread_arg_t arg);
void service_os_thread_delay(unsigned long delay_ms);
void service_os_thread_delay_until(unsigned long* prev_ms, unsigned long delay_ms);
unsigned long service_os_thread_get_time_count(void);
service_os_res_t service_os_thread_suspend(service_os_thread_handle_t handle);
service_os_res_t service_os_thread_resume(service_os_thread_handle_t handle);
service_os_res_t service_os_thread_delete(service_os_thread_handle_t handle);
service_os_res_t service_os_thread_wait_for(service_os_thread_handle_t handle);
service_os_thread_handle_t service_os_thread_find(char* name);
service_os_res_t service_os_is_inside_interrupt(void);
void* service_os_malloc(size_t size);
void service_os_free(void* buff);
service_os_mutex_handle_t service_os_mutex_create(void);
service_os_res_t service_os_mutex_lock(service_os_mutex_handle_t handle, unsigned long timeout_ms);
service_os_res_t service_os_mutex_unlock(service_os_mutex_handle_t handle);
service_os_queue_handle_t service_os_queue_create(size_t len, size_t size);
service_os_res_t service_os_queue_send(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms);
service_os_res_t service_os_queue_receive(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms);
void service_os_queue_delete(service_os_queue_handle_t handle);


#endif
