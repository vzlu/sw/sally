# SALLY - The Service Abstraction Layer

## Overview

SALLY is an abstraction layer intended to separate services of the VZLU on-board service stack (DataKeeper, Planner, etc.) from the underlying platform (NanoMind A3200, MicroCore S700, etc.).

The abstraction is based on a common C `struct` that describes the service, and a set of functions that operate on this description. It is up to the user to provide the description, e.g. implement some callbacks in the platform-specific domain.

Furthermore, SALLY can also be used to implement platform/mission-specific services, which can then be reused later.

## Dependecies

SALLY depends on libcsp and libpayloads being eavailable somewhere in the project that uses SALLY. In addition, some of the services using SALLY may have their own dependencies, these are usually to OS, filesystems and other services.

## Structure

* **service.h/service.c**:  
  The core of the abstraction layer, these files contain the definition of the service description struct `service_desc_t`. It also contain definitions and implementations of the functions that operate with the service descriptions provided by the user. Finally, these files also contains some useful macros.

* **service_filesystem.h**:
  Filesystem abstraction, to provide common interface between the abstracted services and different filesystems (FATFS, YAFFS, POSIX etc.).

* **service_os.h/service_os.c**:
  Operating system abstraction, to provide common interface between the abstracted services and different OSs (FreeRTOS, Linux etc.).

* **service_com.h**:
  Communication interface abstraction, to provide common interface between the abstracted services and communication ports (tty, MCU UARTs etc.).

* **csp_pld_txn.h/csp_pld_txn.c**:  
  These files contain some commonly used functions for CSP transactions using the payload specification from libpayloads.

* **service_err_codes.h**:  
  A common set of error codes for basic sanity checks.
  
## Service porting guide

1. Add SALLY to your project as a submodule. The submodule should be placed in `<project_root>/services/`.
2. Add the services you wish to port as submodules. Again, the submodules should be placed in `<project_root>/services/`.
3. Link SALLY to your project. For example, to use sally for DataKeeper in CMake project: `target_link_libraries(<name_of_target> srv-abs dk)`.
4. Somewhere in your project, create the service descriptions. As an example, the snippet below is a service description of DataKeeper from VZLU S700 OBC:
   
```c
#include <dk/dk_defines.h>
#include <dk/task_dk.h>
#include <dk/dk_functions.h>

// must be provided by user!
extern service_fs_t backend_fs_ff;

int datakeeper_post_init() {
  dk_set_fs_backend(&backend_fs_ff);
  return(SERVICE_ERR_NONE);
}

service_desc_t service_dk = {
  .port = DK_SERVICE_PORT,
  .th_top = {
    .attr = {
      .name = "DK",
      .stack_size = 6*256*sizeof(uint32_t),
      .priority = osPriorityNormal,
    },
    .arg = NULL,
    .func = vTaskDatakeeper,
    .id = NULL,
    .active = false,
  },
  .th_spawned = SERVICE_THREAD_NONE,
  .log_flag_index = { 2, 3 },
  .cb_log_setup = { dk_set_logging, dk_internal_set_logging },
  .cb_conn_accepted = NULL,
  .cb_packet_read = NULL,
  .cb_init = NULL,
  .cb_init_args = NULL,
  .cb_reg = dk_register_cb,
  .cbs = { datakeeper_post_init, wdg_feed, NULL, NULL },
  .fs_backend = &backend_fs_ff,
  .fs_register = dk_set_fs_backend,
};
```

In the snippet above, the user provided the following:

* `.th_top.attr.*` - properties of the DataKeeper thread (stack size, priority and name)
* `.log_flag_index` - indexes of logging enable flags
* `.cbs` - two callbacks to implement platform-dependent functionality: `datakeeper_post_init()`, which is called as the last part of DataKeeper initialization, and `wdg_feed()`, which is called during long packet transfers.
* `.fs_backend` - filesystem backend (see below).
  
5. Create an array of all services you want to implement (i.e., the service list). The service list must be NULL-terminated!

```c
service_desc_t* services[] = {
  &service_dk,
  NULL
};
```

6. During initialization phase, spawn the services from service list by calling `service_spawn()` on each item of the service list within a loop. The logic of this spawning is left up to the user. It is recommended to spawn the services in the order in which they appear in the service list and use some stepping mechanism that will allow operator to debug this startup sequence.
7. Call `service_setup_logging()` for each service - this will set up logging as defined by the service description struct. Please note that this will usually require the flags to be pulled from some persistent storage (e.g., the Persist service), which then must be initialzied first!
8. Call `service_register_csp_callbacks()` to set up all CSP callbacks for all of the services within the service list.
9. In the CSP server thread, call `service_exec_accept_callback()` each time a connection is accepted and `service_exec_read_callback()` each time a packet from connection is read. This will pass the connections/packets to the services.

## Filesystems, Operating systems and COM ports

SALLY also provides an abstraction layer for filesystems, OSs and communiction ports, based on simple backends. This allows to decouple services such as DataKeeper or Filesystem from specific filesystem type (FATFS, YAFFS, etc.) or OS (FreeRTOS, Linux, etc.). These backends are kept in a [separate repository](https://gitlab.com/vzlu/sw/sally-backends).
