#include <service/service.h>
#include <service/service_err_codes.h>

#include <string.h>

#if defined(SERVICE_CSP)
#include <csp/csp.h>

// list of callbacks attached to CSP ports
// each position is a callback attached to that port number
// read_cbs[15] will be executed for port 15
on_csp_packet_read_cb_t read_cbs[CSP_ID_PORT_MAX + 1];
on_csp_conn_accepted_cb_t accept_cbs[CSP_ID_PORT_MAX + 1];
#endif

bool service_is_terminator(service_desc_t* service) {
  if(service == NULL) {
    // plain NULL is also considered a terminator - further evaluation is not possible
    return(true);
  }

  // if the service exists, check ALL of the key parameters
  #if defined(SERVICE_CSP)
  if((service->port == SERVICE_CSP_PORT_UNUSED) && 
    (service->th_top.attr == NULL) && 
    (service->th_spawned.attr == NULL) &&
    (service->cb_packet_read == NULL) &&
    (service->cb_init == NULL)) {
      return(true);
    }
  #else
  if((service->th_top.attr == NULL) && 
    (service->th_spawned.attr == NULL) &&
    (service->cb_init == NULL)) {
      return(true);
    }
  #endif
  
  return(false);
}

int service_list_length(service_desc_t** service_list) {
  // just a sanity check
  if(service_list == NULL) {
    return(0);
  }

  // iterate over the list until we hit the terminator
  int len = 0;
  while(!service_is_terminator(service_list[len])) {
    len++;
  }
  return(len);
}

#if defined(SERVICE_CSP)
void service_register_csp_callbacks(service_desc_t** service_list) {
  // just a sanity check
  if(service_list == NULL) {
    return;
  }

  // make sure the callback arrays are empty
  memset(read_cbs, 0x00, (CSP_ID_PORT_MAX + 1)*sizeof(on_csp_packet_read_cb_t));
  memset(accept_cbs, 0x00, (CSP_ID_PORT_MAX + 1)*sizeof(on_csp_packet_read_cb_t));

  for(int i = 0; i < service_list_length(service_list); i++) {
    service_desc_t* service = service_list[i];

    // if the read callback exists, save it to the correct position
    if((service->port > 0) && (service->port <= CSP_ID_PORT_MAX) && (service->cb_packet_read != NULL)) {
      read_cbs[service->port] = service->cb_packet_read;
    }

    // if the accept callback exists, save it to the correct position
    if((service->port > 0) && (service->port <= CSP_ID_PORT_MAX) && (service->cb_conn_accepted != NULL)) {
      accept_cbs[service->port] = service->cb_conn_accepted;
    }
  }
}
#endif

void service_setup_logging(service_desc_t* service, uint32_t flags) {
  // setup logging
  for(uint32_t i = 0; i < SERVICE_LOGGING_LEVELS_MAX; i++) {
    // log level must be used and there must be a log setup callback
    if((service->log_flag_index[i] != SERVICE_LOGGING_LEVEL_UNUSED) && (service->cb_log_setup[i] != NULL)) {
      service->cb_log_setup[i](flags & 1UL << service->log_flag_index[i]);
    }
  }
}

#if defined(SERVICE_CSP)
bool service_exec_read_callback(int port, csp_packet_t* packet, csp_conn_t* conn) {
  // sanity check for negative or out-of-range values (special cases)
  if((port < 0) || (port > CSP_ID_PORT_MAX)) {
    return(false);
  }

  // if the callback exists, execute it
  if(read_cbs[port] != NULL) {
    read_cbs[port](packet, conn);
    return(true);
  }

  return(false);
}

bool service_exec_accept_callback(csp_conn_t* conn) {
  // get the port and check the range
  int port = csp_conn_dport(conn);
  if((port < 0) || (port > CSP_ID_PORT_MAX)) {
    return(false);
  }

  // if the callback exists, execute it
  if(accept_cbs[port] != NULL) {
    accept_cbs[port](conn);
    return(true);
  }

  return(false);
}
#endif

int service_spawn(service_desc_t* service) {
  // spawn the top-level thread (if it exists)
  if(service->th_top.attr != NULL) {
    int res = service_os_thread_create(&service->th_top.handle, service->th_top.attr, service->th_top.func, service->th_top.arg);
    if(res != SERVICE_OS_PASS) {
      return(SERVICE_ERR_THREAD_SPAWN_FAILED);
    }
  }

  // register external callbacks
  for(uint32_t i = 0; i < SERVICE_REG_CB_MAX; i++) {
    if(service->cb_reg != NULL) {
      service->cb_reg(service->cbs[i], i);
    }
  }

  // set the filesystem backend, if any
  if(service->fs_backend) {
    service->fs_register(service->fs_backend);
  }

  // execute the initialization callback
  if(service->cb_init != NULL) {
    service->cb_init(service->cb_init_args);
  }

  return(SERVICE_ERR_NONE);
}

int service_exec_cb(cb_execution_func_t* cbs, int index) {
  if(cbs[index] != NULL) {
    return(cbs[index](NULL));
  }
  return(SERVICE_ERR_CB_NOT_FOUND);
}

int service_exec_cb_args(cb_execution_func_t* cbs, int index, void** args) {
  if(cbs[index] != NULL) {
    return(cbs[index](args));
  }
  return(SERVICE_ERR_CB_NOT_FOUND);
}
