#!/bin/bash

mkdir -p src
mkdir -p include

rm src/*
rm include/*

lpldgen --dest-inc include --dest-src src
mkdir -p include/pld
mv include/payloads.h include/pld/payloads.h
mv include/payloads_internal.h include/pld/payloads_internal.h
