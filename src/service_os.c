#include <service/service_os.h>
#include <service/service_err_codes.h>

// this is a "singleton", because it doesn't seem to make sense to run on multiple OS's at once
static service_os_t* os_backend = NULL;

void service_set_os(service_os_t* os) {
  os_backend = os;
}

service_os_res_t service_os_thread_create(service_os_thread_handle_t* handle, service_os_thread_attr_t attr, void* func, void* arg) {
  if(!os_backend || !os_backend->thread_create) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->thread_create(handle, attr, func, arg));
}

void service_os_thread_delay(unsigned long delay_ms) {
  if(!os_backend || !os_backend->thread_delay) {
    return;
  }
  os_backend->thread_delay(delay_ms);
}

void service_os_thread_delay_until(unsigned long* prev_ms, unsigned long delay_ms) {
  if(!os_backend || !os_backend->thread_delay_until) {
    return;
  }
  os_backend->thread_delay_until(prev_ms, delay_ms);
}

unsigned long service_os_thread_get_time_count(void) {
  if(!os_backend || !os_backend->thread_get_time_count) {
    return(0);
  }
  return(os_backend->thread_get_time_count());
}

service_os_res_t service_os_thread_suspend(service_os_thread_handle_t handle) {
  if(!os_backend || !os_backend->thread_suspend) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->thread_suspend(handle));
}

service_os_res_t service_os_thread_resume(service_os_thread_handle_t handle) {
  if(!os_backend || !os_backend->thread_resume) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->thread_resume(handle));
}

service_os_res_t service_os_thread_delete(service_os_thread_handle_t handle) {
  if(!os_backend || !os_backend->thread_delete) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->thread_delete(handle));
}

service_os_res_t service_os_thread_wait_for(service_os_thread_handle_t handle) {
  if(!os_backend || !os_backend->thread_wait_for) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->thread_wait_for(handle));
}

service_os_thread_handle_t service_os_thread_find(char* name) {
  if(!os_backend || !os_backend->thread_find) {
    return(NULL);
  }
  return(os_backend->thread_find(name));
}

 service_os_res_t service_os_is_inside_interrupt(void) {
  if(!os_backend || !os_backend->is_inside_interrupt) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->is_inside_interrupt());
}

void* service_os_malloc(size_t size) {
  if(!os_backend || !os_backend->malloc) {
    return(NULL);
  }
  return(os_backend->malloc(size));
}

void service_os_free(void* buff) {
  if(!os_backend || !os_backend->free) {
    return;
  }
  return(os_backend->free(buff));
}

service_os_mutex_handle_t service_os_mutex_create(void) {
  if(!os_backend || !os_backend->mutex_create) {
    return(NULL);
  }
  return(os_backend->mutex_create());
}

service_os_res_t service_os_mutex_lock(service_os_mutex_handle_t handle, unsigned long timeout_ms) {
  if(!os_backend || !os_backend->mutex_lock) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->mutex_lock(handle, timeout_ms));
}

service_os_res_t service_os_mutex_unlock(service_os_mutex_handle_t handle) {
  if(!os_backend || !os_backend->mutex_unlock) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->mutex_unlock(handle));
}

service_os_queue_handle_t service_os_queue_create(size_t len, size_t size) {
  if(!os_backend || !os_backend->queue_create) {
    return(NULL);
  }
  return(os_backend->queue_create(len, size));
}

service_os_res_t service_os_queue_send(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms) {
  if(!os_backend || !os_backend->queue_send) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->queue_send(handle, item, timeout_ms));
}

service_os_res_t service_os_queue_receive(service_os_queue_handle_t handle, void* item, unsigned long timeout_ms) {
  if(!os_backend || !os_backend->queue_receive) {
    return(SERVICE_OS_FAIL);
  }
  return(os_backend->queue_receive(handle, item, timeout_ms));
}

void service_os_queue_delete(service_os_queue_handle_t handle) {
  if(!os_backend || !os_backend->queue_delete) {
    return;
  }
  os_backend->queue_delete(handle);
}
